import { TempService } from './../temp.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
likes:number=0;
temperature;
city;
image:String;
$tempData:Observable<Weather>;
errorMessage:String;
hasError:Boolean = false;

addLikes(){
  this.likes++
};

  constructor(private route: ActivatedRoute, private Tempservice:TempService) { }
  ngOnInit() {
  this.city= this.route.snapshot.params.city;
  this.$tempData= this.Tempservice.searchWeatherData(this.city);
  this.$tempData.subscribe(
    data => {
      console.log(data);
      this.temperature = data.temperature;
      this.image = data.image;
    },
    error =>{
      this.hasError = true;
      this.errorMessage = error.message;
      console.log('in the component '+error.message);
    }
  )
  //this.temperature = this.route.snapshot.params.temp;
}}






import { WeatherRaw } from './interfaces/weather-raw';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Weather } from './interfaces/weather';
import {map, catchError} from  'rxjs/operators';




@Injectable({
  providedIn: 'root'
})
export class TempService {

  private URL="https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY="73603b79d09bec086edcd24422c1b248";
  private IMP = "&units=metric"; // רוצה את הנתונים ביחידות מטריות - למשל צלזיוס ולא פארנהייט

  searchWeatherData(cityName:String):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
    .pipe(map(data => this.transformWeatherData(data)),
    catchError(this.handleError)
    )
  }
  private handleError(res:HttpErrorResponse){
      //console.log(res.error);
      return throwError(res.error)
  }
  private transformWeatherData(data:WeatherRaw):Weather{
    return {
      name:data.name,
      country:data.sys.country,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description: data.weather[0].description,
      temperature: data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon
    }
    
  }
  constructor(private http:HttpClient) { }

}